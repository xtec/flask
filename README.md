# README

```sh
docker run --rm -d --name flask -p 80:80 registry.gitlab.com/xtec/flask
```


## Virtual environment

Install virtual environment

```sh
$ sudo apt install python3-venv
```

Activate `venv` and install dependencies

```sh
$ python3 -m venv .venv
$ source .venv/bin/activate
(.venv) $ python3 -m pip install --upgrade pip
(.venv) $ pip install -r requirements-dev.txt
```

Start the web app:

Debug mode:  https://flask.palletsprojects.com/en/2.3.x/server/

flask run --debug -h 0.0.0.0

```sh
$ flask run -h 0.0.0.0
* Environment: production
...
* Running on all addresses (0.0.0.0)
* Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
```

Fire up a web browser and navigate to [http://localhost:5000](http://localhost:5000)


## Docker

Build a docker image:

```sh
docker build -t xtec/flask .
```

Run the `xtec/flask` image:

```sh
docker run --rm -d --name flask -p 5000:80 xtec/flask
``` 

## Azure

TODO !

## Google cloud

TODO!

You can browse this project hosted on Google Cloud: [https://webapp-python3.ew.r.appspot.com](https://webapp-python3.ew.r.appspot.com)

You can deploy this app to [Google Cloud](https://console.cloud.google.com).

First, create a project:

```sh
$ gcloud auth login
$ gcloud projects create <project-id>
$ gcloud config set project <project-id>
```

Deploy the app:

```sh
$ gcloud app deploy
```



